//
//  FormViewController.swift
//  iOSdemoapp
//
//  Created by rotake on 2021/03/11.
//  Copyright © 2021 rotake. All rights reserved.
//

import UIKit

extension UITextField{
    func addBorderBottom(height: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
}

class FormViewController: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameText.placeholder = "NAME"
        nameText.addBorderBottom(height: 1.0, color: UIColor.lightGray)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
