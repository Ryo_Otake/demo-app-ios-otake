//
//  MapViewController.swift
//  iOSdemoapp
//
//  Created by rotake on 2021/03/11.
//  Copyright © 2021 rotake. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController,CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    lazy var mapView = GMSMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        //初期値は東京
        let camera = GMSCameraPosition.camera(withLatitude: 35.681223, longitude: 139.767059, zoom: 17.0)
        mapView = GMSMapView.map(withFrame: CGRect(origin: .zero, size: view.bounds.size), camera: camera)
        
        
//        locationManager.requestWhenInUseAuthorization()
//        switch CLLocationManager.authorizationStatus() {
//        case .notDetermined, .restricted, .denied:
//            break
//        case .authorizedAlways, .authorizedWhenInUse:
//            mapView.settings.myLocationButton = true //右下のボタン追加する
//            mapView.isMyLocationEnabled = true
//
//        @unknown default:
//            break
//        }
//
        
        
        
        locationManager.startUpdatingLocation()
        
        // User Location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        if locationManager.delegate == {
//            mapView.settings.myLocationButton = true //右下のボタン追加する
//            mapView.isMyLocationEnabled = true
//        }

        self.view.addSubview(mapView)
        self.view.insertSubview(mapView, at:0)
        
//        locationManager.stopUpdatingLocation()
        
    }
    
    // 位置情報を取得・更新するたびに呼ばれる
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last

        let camera = GMSCameraPosition.camera(
            withLatitude: userLocation!.coordinate.latitude,
            longitude: userLocation!.coordinate.longitude,
            zoom: 17.0)
        self.mapView.animate(to: camera)
    }
}

extension MapViewController{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
    switch status {
    case .authorizedWhenInUse, .authorizedAlways:
        // 許可ボタンをタップしたとき
        mapView.settings.myLocationButton = true //右下のボタン追加する
        mapView.isMyLocationEnabled = true
    default:
        break
        }
    }
}
