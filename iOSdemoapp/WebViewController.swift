//
//  WebViewController.swift
//  iOSdemoapp
//
//  Created by rotake on 2021/03/11.
//  Copyright © 2021 rotake. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    private let sonixURLText =  "https://www.sonix.asia/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        guard let url = URL(string: sonixURLText) else {
            return
        }
        
            /* URL文字列の表記間違いなどで、URL()がnilになる場合があるため、nilにならない場合のみ以下のload()が実行されるようにしている*/
        
        webView.load(URLRequest(url: url))
        webView.navigationDelegate = self
    }
}

extension WebViewController {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //ヘッダーをサイトのタイトルに変更
        if let title = webView.title {
            navigationItem.title = title
        }
    }
}
