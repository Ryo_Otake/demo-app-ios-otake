//
//  DemoAppTopTableViewController.swift
//  iOSdemoapp
//
//  Created by rotake on 2021/03/11.
//  Copyright © 2021 rotake. All rights reserved.
//

import UIKit

class DemoAppTopTableViewController: UITableViewController {

    //widjetのデータ
    var widjets = ["Input Text","Slider","Drag and Drop","Map View","Play Video","Web View","View Pager","Grid View","Unquete","Scroll"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        
        self.navigationController!.navigationBar.barStyle = .black
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 0.0, green: 0.55, blue: 1.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = .white
        self.navigationController!.navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.white
        ]
        
    }

    // MARK: - Table view data source
    
    //セクション数
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }

    //行数
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.widjets.count
    }

    //表示
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DemoAppTopWidjet", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = self.widjets[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            /// 遷移先セグエIdentifier
                    switch indexPath.row {
                    case 0: break
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
//                            performSegue(withIdentifier: "SliderSegue", sender: nil)
                        case 1:
                            // 数学Iのセルタップ時処理
                            // 以下はセグエで数学Iの画面遷移する場合の例
                            performSegue(withIdentifier: "SliderSegue", sender: nil)
                        case 2: break
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        
                        case 3: 
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        performSegue(withIdentifier: "MapSegue", sender: nil)
                        case 4:
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        performSegue(withIdentifier: "VideoSegue", sender: nil)
                        case 5:
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        performSegue(withIdentifier: "WebSegue", sender: nil)
                        case 6:
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        performSegue(withIdentifier: "PagerSegue", sender: nil)
                        case 7: break
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
//                        performSegue(withIdentifier: "FormSegue", sender: nil)
                        case 8:
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
                        performSegue(withIdentifier: "FormSegue", sender: nil)
                        case 9: break
                        // 数学Iのセルタップ時処理
                        // 以下はセグエで数学Iの画面遷移する場合の例
//                        performSegue(withIdentifier: "SliderSegue", sender: nil)
                    default:
                        break
        }
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    }
*/
    }

