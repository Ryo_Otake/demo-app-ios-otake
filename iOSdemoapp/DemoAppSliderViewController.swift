//
//  DemoAppSliderViewController.swift
//  iOSdemoapp
//
//  Created by rotake on 2021/03/11.
//  Copyright © 2021 rotake. All rights reserved.
//

import UIKit

final class DemoAppSliderViewController: UIViewController {
    @IBOutlet weak var ColorView: UIView!
    @IBOutlet weak var RedSlider: UISlider!
    @IBOutlet weak var BlueSlider: UISlider!
    @IBOutlet weak var GreenSlider: UISlider!
    
    
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        print("Color: \(sender.value)")
        changeViewBackgroundColor(red: RedSlider.value,
                                  green: GreenSlider.value,
                                  blue: BlueSlider.value)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func changeViewBackgroundColor(red: Float, green: Float, blue: Float) {
        ColorView.backgroundColor = UIColor(
            red: CGFloat(red) / 255,
            green: CGFloat(green) / 255,
            blue: CGFloat(blue) / 255,
            alpha: 1.0)
    }
}

